﻿using FluentValidation.TestHelper;
using Freelance.Mailtrap.Utilities.DataModels;
using Freelance.Mailtrap.Utilities.FluentValidation;


namespace Freelance.Mailtrap.Utilities.Tests.FluentValidation;

public class EmailRecipientValidatorTests
{
    private readonly EmailRecipientValidator _validator;

    public EmailRecipientValidatorTests()
    {
        _validator = new EmailRecipientValidator();
    }

    [Fact]
    public void Validate_ValidEmailRecipient_ReturnsNoErrors()
    {
        // Arrange
        var emailRecipient = new EmailRecipient
        {
            Email = "test@example.com",
            Name = "Test Name"
        };

        // Act
        var result = _validator.TestValidate(emailRecipient);

        // Assert
        result.ShouldNotHaveAnyValidationErrors();
    }

    [Fact]
    public void Validate_InvalidEmail_ReturnsError()
    {
        // Arrange
        var emailRecipient = new EmailRecipient
        {
            Email = "invalid email",
            Name = "Test Name"
        };

        // Act
        var result = _validator.TestValidate(emailRecipient);

        // Assert
        result.ShouldHaveValidationErrorFor(r => r.Email);
    }

    [Fact]
    public void Validate_NullName_ReturnsError()
    {
        // Arrange
        var emailRecipient = new EmailRecipient
        {
            Email = "test@example.com",
            Name = null
        };

        // Act
        var result = _validator.TestValidate(emailRecipient);

        // Assert
        result.ShouldHaveValidationErrorFor(r => r.Name);
    }
}