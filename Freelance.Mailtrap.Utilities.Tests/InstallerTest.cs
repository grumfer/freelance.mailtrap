﻿using Freelance.Mailtrap.Utilities.Clients.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;

namespace Freelance.Mailtrap.Utilities.Tests;

public class InstallerTests
{
    private readonly ServiceCollection _services;
    private readonly IConfiguration _configuration;

    public InstallerTests()
    {
        _services = new ServiceCollection();
        _configuration = new ConfigurationBuilder().AddInMemoryCollection(new Dictionary<string, string>
        {
            { "RetryPolicy:RetryCount", "3" }
        }!).Build();
    }

    [Fact]
    public void RegisterCommonMailtrapUtilitiesDependencies_RegistersDependenciesCorrectly()
    {
        // Act
        _services.RegisterCommonMailtrapUtilitiesDependencies(_configuration);

        // Assert

        Assert.Contains(_services, service => service.ServiceType == typeof(IMailtrapClient) && service.Lifetime == ServiceLifetime.Transient);
        Assert.Contains(_services, service => service.ServiceType == typeof(IAsyncPolicy<HttpResponseMessage>) && service.Lifetime == ServiceLifetime.Singleton);
    }
}