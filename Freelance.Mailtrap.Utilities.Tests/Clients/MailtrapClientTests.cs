﻿using AutoFixture;
using FluentValidation;
using FluentValidation.Results;
using Freelance.Mailtrap.Utilities.Clients;
using Freelance.Mailtrap.Utilities.Configuration.Contracts;
using Freelance.Mailtrap.Utilities.DataModels;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using Polly;
using System.Net;
using System.Text;
using System.Text.Json;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace Freelance.Mailtrap.Utilities.Tests.Clients;

public class MailtrapClientTests
{
    private readonly Mock<IConfigurationManager> _configurationManagerMock;
    private readonly Mock<IValidator<MailtrapRequestBody>> _validatorMock;
    private readonly Mock<ILogger<MailtrapClient>> _loggerMock;
    private readonly HttpClient _httpClient;
    private readonly IAsyncPolicy<HttpResponseMessage> _retryPolicy;
    private readonly MailtrapClient _mailtrapClient;
    private readonly IFixture _fixture;

    public MailtrapClientTests()
    {
        _httpClient = new HttpClient();
        _retryPolicy = Policy.NoOpAsync<HttpResponseMessage>();
        _configurationManagerMock = new Mock<IConfigurationManager>();
        _validatorMock = new Mock<IValidator<MailtrapRequestBody>>();
        _loggerMock = new Mock<ILogger<MailtrapClient>>();
        _mailtrapClient = new MailtrapClient(_httpClient, _retryPolicy, _configurationManagerMock.Object, _validatorMock.Object, _loggerMock.Object);
        _fixture = new Fixture();
    }

    [Fact]
    public async Task ValidateAndSendRequestAsync_ValidRequest_ValidationInvalid()
    {
        // Arrange
        var requestBody = _fixture.Create<MailtrapRequestBody>();

        var validationFailure = new ValidationFailure(It.IsAny<string>(), It.IsAny<string>());
        var validationResult = new ValidationResult(new List<ValidationFailure> { validationFailure });

        _validatorMock.Setup(v => v.ValidateAsync(requestBody, default))
            .ReturnsAsync(validationResult);

        // Act
        var exception = await Assert.ThrowsAsync<ValidationException>(() => _mailtrapClient.ValidateAndSendRequestAsync(requestBody));

        // Assert
        Assert.IsType<ValidationException>(exception);
        _validatorMock.Verify(v => v.ValidateAsync(requestBody, default), Times.Once);
        _loggerMock.Verify(l => l.Log(
            LogLevel.Error,
            It.IsAny<EventId>(),
            It.Is<It.IsAnyType>((v, t) => true),
            It.IsAny<Exception>(),
            It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)), Times.Once);
    }

    [Fact]
    public async Task SendRequestAsync_ValidRequest_ReturnsExpectedResponse()
    {
        // Arrange
        var requestBody = _fixture.Create<MailtrapRequestBody>();
        var expectedResponse = _fixture.Create<MailtrapResponse>();
        var expectedResponseJson = JsonSerializer.Serialize(expectedResponse);

        var handlerMock = new Mock<HttpMessageHandler>();
        handlerMock
            .Protected()
            .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            )
            .ReturnsAsync(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(expectedResponseJson, Encoding.UTF8, "application/json")
            });

        var httpClient = new HttpClient(handlerMock.Object)
        {
            BaseAddress = new Uri("http://localhost")
        };
        var mailtrapClient = new MailtrapClient(httpClient, _retryPolicy, _configurationManagerMock.Object, _validatorMock.Object, _loggerMock.Object);

        // Act
        var actualResponse = await mailtrapClient.SendRequestAsync(requestBody);

        // Assert
        Assert.True(expectedResponse.Success == actualResponse.Success
                    && expectedResponse.MessageIds.SequenceEqual(actualResponse.MessageIds));
        handlerMock.Protected().Verify(
            "SendAsync",
            Times.Exactly(1),
            ItExpr.IsAny<HttpRequestMessage>(),
            ItExpr.IsAny<CancellationToken>());
    }

    [Fact]
    public async Task SendRequestAsync_Non200Response_ThrowsExceptionAndLogsError()
    {
        // Arrange
        var requestBody = _fixture.Create<MailtrapRequestBody>();

        var handlerMock = new Mock<HttpMessageHandler>();
        handlerMock
            .Protected()
            .Setup<Task<HttpResponseMessage>>(
                "SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>()
            )
            .ReturnsAsync(new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.BadRequest,
            });

        var httpClient = new HttpClient(handlerMock.Object)
        {
            BaseAddress = new Uri("http://localhost")
        };
        var mailtrapClient = new MailtrapClient(httpClient, _retryPolicy, _configurationManagerMock.Object, _validatorMock.Object, _loggerMock.Object);

        // Act
        var exception = await Assert.ThrowsAsync<HttpRequestException>(() => mailtrapClient.SendRequestAsync(requestBody));

        // Assert
        Assert.IsType<HttpRequestException>(exception);
        _loggerMock.Verify(l => l.Log(
            LogLevel.Error,
            It.IsAny<EventId>(),
            It.Is<It.IsAnyType>((v, t) => true),
            It.IsAny<Exception>(),
            It.Is<Func<It.IsAnyType, Exception?, string>>((v, t) => true)), Times.Once);
    }

}