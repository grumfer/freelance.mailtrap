﻿namespace Freelance.Mailtrap.Utilities.DataModels;

public class MailtrapRequestBody
{
    public EmailRecipient From { get; set; }

    public ICollection<EmailRecipient> To { get; set; }
        
    public ICollection<EmailRecipient>? Cc { get; set; }
        
    public ICollection<EmailRecipient>? Bcc { get; set; }
    
    public ICollection<Attachment>? Attachments { get; set; }
        
    public CustomVariables? CustomVariables { get; set; }
        
    public Headers? Headers { get; set; }
        
    public string? Subject { get; set; }
        
    public string? Text { get; set; }
    
    public string? Html { get; set; }
        
    public string? Category { get; set; }
}