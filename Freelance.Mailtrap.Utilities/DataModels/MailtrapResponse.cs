﻿namespace Freelance.Mailtrap.Utilities.DataModels;

public class MailtrapResponse
{
    public bool Success { get; set; }
        
    public List<string> MessageIds { get; set; }
}