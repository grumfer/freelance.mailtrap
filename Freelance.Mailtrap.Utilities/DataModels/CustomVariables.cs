﻿using System.Text.Json.Serialization;

namespace Freelance.Mailtrap.Utilities.DataModels;

public class CustomVariables
{
    [JsonPropertyName("user_id")]
    public string? UserId { get; set; }

    [JsonPropertyName("batch_id")]
    public string? BatchId { get; set; }
}