﻿namespace Freelance.Mailtrap.Utilities.DataModels;

public class EmailRecipient
{
    public string Email { get; set; }
        
    public string Name { get; set; }
}