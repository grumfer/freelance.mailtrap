﻿using System.Text.Json.Serialization;

namespace Freelance.Mailtrap.Utilities.DataModels;

public class Headers
{
    [JsonPropertyName("X-Message-Source")]
    public string? XMessageSource { get; set; }
}