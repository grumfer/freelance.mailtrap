﻿using FluentValidation;
using Freelance.Mailtrap.Utilities.Constants;
using Freelance.Mailtrap.Utilities.DataModels;

namespace Freelance.Mailtrap.Utilities.FluentValidation;

public class EmailRecipientValidator : AbstractValidator<EmailRecipient>
{
    public EmailRecipientValidator()
    {
        RuleFor(recipient => recipient.Email)
            .NotNull()
            .EmailAddress()
            .MaximumLength(ConstantValues.MaxLength256);

        RuleFor(recipient => recipient.Name)
            .NotNull()
            .MaximumLength(ConstantValues.MaxLength256);
    }
}