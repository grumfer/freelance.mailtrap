﻿using FluentValidation;
using Freelance.Mailtrap.Utilities.Constants;
using Freelance.Mailtrap.Utilities.DataModels;

namespace Freelance.Mailtrap.Utilities.FluentValidation;

internal class AttachmentValidator : AbstractValidator<Attachment>
{
    public AttachmentValidator()
    {
        RuleFor(attachment => attachment.Content)
            .NotNull()
            .NotEmpty();

        RuleFor(attachment => attachment.Filename)
            .NotNull()
            .NotEmpty()
            .MaximumLength(ConstantValues.MaxLength256);
    }
}