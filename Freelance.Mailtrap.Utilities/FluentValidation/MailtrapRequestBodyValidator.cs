﻿using FluentValidation;
using Freelance.Mailtrap.Utilities.Constants;
using Freelance.Mailtrap.Utilities.DataModels;

namespace Freelance.Mailtrap.Utilities.FluentValidation;

internal class MailtrapRequestBodyValidator : AbstractValidator<MailtrapRequestBody>
{
    public MailtrapRequestBodyValidator()
    {
        ClassLevelCascadeMode = CascadeMode.Stop;

        RuleFor(body => body.From)
            .NotNull()
            .SetValidator(new EmailRecipientValidator());

        RuleFor(body => body.To)
            .NotNull()
            .NotEmpty()
            .Must(list => list.Count <= 1000)
            .WithMessage(Messages.ListMore1000ValidationError)
            .ForEach(item => item
                .SetValidator(new EmailRecipientValidator()));

        RuleFor(body => body.Cc)
            .Must(list => list is not { Count: > 1000 })
            .WithMessage(Messages.ListMore1000ValidationError)
            .When(body => body.Cc.Count > default(int))
            .ForEach(item => item
                .SetValidator(new EmailRecipientValidator()));

        RuleFor(body => body.Bcc)
            .Must(list => list is not { Count: > 1000 })
            .WithMessage(Messages.ListMore1000ValidationError)
            .When(body => body.Cc.Count > default(int))
            .ForEach(item => item
                .SetValidator(new EmailRecipientValidator()));
            
        When(body => body.Attachments.Count > default(int), () =>
        {
            RuleForEach(body => body.Attachments)
                .NotNull()
                .SetValidator(new AttachmentValidator());
        });

        RuleFor(body => body.Subject)
            .NotNull()
            .NotEmpty()
            .MaximumLength(ConstantValues.MaxLength256);

        RuleFor(body => body)
            .Must(body => !string.IsNullOrEmpty(body.Text) || !string.IsNullOrEmpty(body.Html))
            .WithMessage(Messages.TextOrHtmlRequiredValidationError);

        RuleFor(body => body.Subject)
            .MaximumLength(ConstantValues.MaxLength256);
    }
}