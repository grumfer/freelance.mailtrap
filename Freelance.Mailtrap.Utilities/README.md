<h1 align="center"> Mailtrap.Utilities </h1> <br>

<p align="center">
  Freelance.Mailtrap.Utilities is a .NET library that provides utility classes and methods for interacting with the Mailtrap API. It includes classes for data models, clients, configuration, constants, and FluentValidation validators.
</p>


## Table of Contents

- [Features](#features)
- [Requirements](#requirements)
- [Quick Start](#quick-start)

## Features

* Data Models for Mailtrap API.
* Client(MailtrapClient) possible to validate and send requests.


## Requirements
The application can be run locally using tools provided below.

* [.NET 8]
* [Visual Studio](https://visualstudio.microsoft.com/downloads/) / [Visual Studio Code](https://code.visualstudio.com/Download) / [JetBrains Rider](https://www.jetbrains.com/rider/)


## Quick Start
If you use DI you can add the following code to your Program.cs file and just use Client via DI:
```csharp
builder.Services.RegisterCommonMailtrapUtilitiesDependencies(builder.Configuration);
```

Or you can use the following code to create a client:
```csharp
var mailtrapClient = new MailtrapClient(httpClient, retryPolicy, configurationManager, validator, logger);
```
