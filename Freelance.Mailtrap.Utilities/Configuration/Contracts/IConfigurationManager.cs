﻿namespace Freelance.Mailtrap.Utilities.Configuration.Contracts;

public interface IConfigurationManager
{
    string JWTToken { get; }

    string ApiToken { get; }

    string DefaultHost { get; }
}