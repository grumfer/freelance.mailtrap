﻿using Microsoft.Extensions.Configuration;
using IConfigurationManager = Freelance.Mailtrap.Utilities.Configuration.Contracts.IConfigurationManager;

namespace Freelance.Mailtrap.Utilities.Configuration;

internal class ConfigurationManager(IConfiguration configuration) : IConfigurationManager
{
    public string JWTToken => configuration["Mailtrap:JWTToken"]!;
        
    public string ApiToken => configuration["Mailtrap:ApiToken"]!;
        
    public string DefaultHost => configuration["Mailtrap:DefaultHost"]!;
}