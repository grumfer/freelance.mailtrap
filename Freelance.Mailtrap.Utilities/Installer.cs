﻿using FluentValidation;
using Freelance.Mailtrap.Utilities.Clients;
using Freelance.Mailtrap.Utilities.Clients.Contracts;
using Freelance.Mailtrap.Utilities.FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Polly;
using ConfigurationManager = Freelance.Mailtrap.Utilities.Configuration.ConfigurationManager;
using IConfigurationManager = Freelance.Mailtrap.Utilities.Configuration.Contracts.IConfigurationManager;

namespace Freelance.Mailtrap.Utilities;

public static class Installer
{
    public static IServiceCollection RegisterCommonMailtrapUtilitiesDependencies(
        this IServiceCollection services,
        IConfiguration configuration)
    {
        services
            .RegisterUtilities()
            .RegisterConfigurationDependencies()
            .RegisterPollyDependencies(configuration)
            .RegisterValidators();

        return services;
    }

    private static IServiceCollection RegisterUtilities(
        this IServiceCollection services)
    {
        services.AddTransient<IMailtrapClient, MailtrapClient>();
        
        
        return services;
    }

    public static IServiceCollection RegisterConfigurationDependencies(this IServiceCollection services)
    {
        services.AddSingleton<IConfigurationManager, ConfigurationManager>();

        return services;
    }

    public static IServiceCollection RegisterPollyDependencies(this IServiceCollection services,
        IConfiguration configuration)
    {
        var retryCount = configuration.GetValue<int>("RetryPolicy:RetryCount");

        services.AddHttpClient();
        services.AddSingleton<IAsyncPolicy<HttpResponseMessage>>(Policy.Handle<HttpRequestException>()
            .OrResult<HttpResponseMessage>(r => 
                r.StatusCode != System.Net.HttpStatusCode.OK)
            .WaitAndRetryAsync(retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))));

        return services;
    }

    private static IServiceCollection RegisterValidators(this IServiceCollection services)
    {
        services.AddValidatorsFromAssemblyContaining<MailtrapRequestBodyValidator>();

        return services;
    }
}