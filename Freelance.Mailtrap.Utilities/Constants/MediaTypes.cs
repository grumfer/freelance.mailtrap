﻿namespace Freelance.Mailtrap.Utilities.Constants;

internal static class MediaTypes
{
    internal const string ApplicationJson = "application/json";
}