﻿namespace Freelance.Mailtrap.Utilities.Constants;

internal static class Messages
{
    internal const string SendingRequestError = "Error sending request:";

    internal const string SendingRequestBody = "Send request with body:";

    internal static string ResponseReceived = "Response received with body:";

    internal const string ListMore1000ValidationError = 
        "The list cannot contain more than 1000 elements.";

    internal const string TextOrHtmlRequiredValidationError = 
        "At least Text ot Html is required!";

    internal const string ValidationErrorsFormat =
        "Validation Errors: {0}\nBody: {1}";
}