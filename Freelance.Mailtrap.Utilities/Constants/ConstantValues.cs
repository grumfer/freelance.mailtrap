﻿namespace Freelance.Mailtrap.Utilities.Constants;

internal static class ConstantValues
{
    internal const int MaxLength256 = 256;
}