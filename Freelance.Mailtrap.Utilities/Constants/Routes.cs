﻿namespace Freelance.Mailtrap.Utilities.Constants;

internal static class Routes
{
    internal const string Send = "/api/send";
}