﻿using Freelance.Mailtrap.Utilities.Clients.Contracts;
using Freelance.Mailtrap.Utilities.DataModels;
using Polly;
using System.Text;
using System.Text.Json;
using Freelance.Mailtrap.Utilities.Configuration.Contracts;
using System.Net.Http.Headers;
using Freelance.Mailtrap.Utilities.Constants;
using FluentValidation;
using Microsoft.Extensions.Logging;

namespace Freelance.Mailtrap.Utilities.Clients;

public class MailtrapClient : IMailtrapClient
{
    private readonly HttpClient _httpClient;
    private readonly IAsyncPolicy<HttpResponseMessage> _retryPolicy;
    private readonly IConfigurationManager _configurationManager;
    private readonly IValidator<MailtrapRequestBody> _validator;
    private readonly ILogger<MailtrapClient> _logger;

    public MailtrapClient(
        HttpClient httpClient,
        IAsyncPolicy<HttpResponseMessage> retryPolicy,
        IConfigurationManager configurationManager, 
        IValidator<MailtrapRequestBody> validator,
        ILogger<MailtrapClient> logger)
    {
        _httpClient = httpClient;
        _retryPolicy = retryPolicy;
        _configurationManager = configurationManager;
        _validator = validator;
        _logger = logger;
    }

    public async Task<MailtrapResponse> ValidateAndSendRequestAsync(
        MailtrapRequestBody requestBody,
        CancellationToken cancellationToken = default)
    {
        var validationResult = await _validator.ValidateAsync(requestBody, cancellationToken);
        if (!validationResult.IsValid)
        {
            _logger.LogError(string.Format(
                Messages.ValidationErrorsFormat,
                validationResult.Errors,
                JsonSerializer.Serialize(requestBody))); 
            throw new ValidationException(validationResult.Errors);
        }

        return await SendRequestAsync(requestBody, cancellationToken);
    }

    public async Task<MailtrapResponse> SendRequestAsync(
        MailtrapRequestBody requestBody, 
        CancellationToken cancellationToken = default)
    {
        var json = JsonSerializer.Serialize(requestBody);
        
        _logger.LogInformation($"{Messages.SendingRequestBody} {json}");
        
        var data = new StringContent(json, Encoding.UTF8, MediaTypes.ApplicationJson);

        _httpClient.DefaultRequestHeaders.Authorization = 
            new AuthenticationHeaderValue("Bearer", _configurationManager.JWTToken);

        var response = await _retryPolicy.ExecuteAsync(() => _httpClient.PostAsync(
            $"{_configurationManager.DefaultHost}{Routes.Send}", data, cancellationToken));

        if (!response.IsSuccessStatusCode)
        {
            var message = $"{Messages.SendingRequestError} {response.StatusCode}";
            
            _logger.LogError(message);
            throw new HttpRequestException(message);
        }

        var responseContent = await response.Content.ReadAsStringAsync(cancellationToken);

        _logger.LogInformation($"{Messages.ResponseReceived} {responseContent}");
        
        return JsonSerializer.Deserialize<MailtrapResponse>(responseContent);
    }
}