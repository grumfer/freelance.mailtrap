﻿using Freelance.Mailtrap.Utilities.DataModels;

namespace Freelance.Mailtrap.Utilities.Clients.Contracts;

public interface IMailtrapClient
{
    Task<MailtrapResponse> ValidateAndSendRequestAsync(
        MailtrapRequestBody requestBody,
        CancellationToken cancellationToken = default);
    
    Task<MailtrapResponse> SendRequestAsync(
        MailtrapRequestBody requestBody,
        CancellationToken cancellationToken = default);
}