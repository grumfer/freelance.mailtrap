using Freelance.Mailtrap.Utilities;
using Freelance.Mailtrap.WebApi.BusinessLogic.Services;
using Freelance.Mailtrap.WebApi.BusinessLogic.Services.Contracts;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IMailService, MailService>();
builder.Services.RegisterCommonMailtrapUtilitiesDependencies(builder.Configuration);

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
