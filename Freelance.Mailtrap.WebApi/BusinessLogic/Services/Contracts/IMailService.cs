﻿using Freelance.Mailtrap.Utilities.DataModels;

namespace Freelance.Mailtrap.WebApi.BusinessLogic.Services.Contracts;

public interface IMailService
{
    Task<MailtrapResponse> SendMailAsync(
        MailtrapRequestBody body,
        CancellationToken cancellationToken = default);

    Task<MailtrapResponse> ValidateAndSendMailAsync(
        MailtrapRequestBody body,
        CancellationToken cancellationToken = default);
}