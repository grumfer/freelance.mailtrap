﻿using Freelance.Mailtrap.Utilities.Clients.Contracts;
using Freelance.Mailtrap.Utilities.DataModels;
using Freelance.Mailtrap.WebApi.BusinessLogic.Services.Contracts;

namespace Freelance.Mailtrap.WebApi.BusinessLogic.Services;

public class MailService : IMailService
{
    private readonly IMailtrapClient _mailtrapClient;

    public MailService(IMailtrapClient mailtrapClient)
    {
        _mailtrapClient = mailtrapClient;
    }

    public Task<MailtrapResponse> SendMailAsync(
        MailtrapRequestBody body,
        CancellationToken cancellationToken = default)
    {
        return _mailtrapClient.SendRequestAsync(body, cancellationToken);
    }

    public Task<MailtrapResponse> ValidateAndSendMailAsync(
        MailtrapRequestBody body,
        CancellationToken cancellationToken = default)
    {
        return _mailtrapClient.ValidateAndSendRequestAsync(body, cancellationToken);
    }
}