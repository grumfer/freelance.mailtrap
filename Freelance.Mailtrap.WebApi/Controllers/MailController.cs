using Freelance.Mailtrap.Utilities.DataModels;
using Freelance.Mailtrap.WebApi.BusinessLogic.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace Freelance.Mailtrap.WebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class MailController : ControllerBase
{
    private readonly ILogger<MailController> _logger;
    private readonly IMailService _mailService;

    public MailController(ILogger<MailController> logger, IMailService mailService)
    {
        _logger = logger;
        _mailService = mailService;
    }

    [HttpPost]
    public async Task<IActionResult> SendMailAsync([FromBody] MailtrapRequestBody body)
    {
        var result = await _mailService.SendMailAsync(body);
        return Ok(result);
    }

    [HttpPost("validate-and-send")]
    public async Task<IActionResult> ValidateAndSendMailAsync([FromBody] MailtrapRequestBody body)
    {
        var result = await _mailService.ValidateAndSendMailAsync(body);
        return Ok(result);
    }
}